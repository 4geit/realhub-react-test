import React, { Component } from 'react';
import { ListItem } from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import IconMenu from 'material-ui/IconMenu';
import moment from 'moment';
import data from './comment_data.json';
import { blue, darkGrey, lightGrey, lineColor, linkColor } from './colors.json';

const Divider = () => (
  <hr style={{
    margin: "0 10px",
    border: "none",
    borderBottomColor: lineColor,
    borderBottomStyle: "dashed",
    borderBottomWidth: "2px"
  }}/>
);

class MarkAsSeenLink extends Component {
  constructor(props) {
    super(props);
    this.handleMarkAsSeenLinkClick = this.handleMarkAsSeenLinkClick.bind(this);
  }

  handleMarkAsSeenLinkClick(e) {
    // change acknowledged state to true
    this.props.item.acknowledged = true;
    // trigger the upstream ListMessagesContainer event
    this.props.onMarkAsSeenLink();
  }

  render() {
    return (
      <span>
        &nbsp;|&nbsp;
        <a
          style={{
            textDecoration: "none",
            color: linkColor
          }}
          onClick={this.handleMarkAsSeenLinkClick}
        >Mark as Seen</a>
      </span>
    );
  }
}

class ListMessagesContainer extends Component {
  constructor(props) {
    super(props);
    // define event callback
    this.handleMarkAsSeenLink = this.handleMarkAsSeenLink.bind(this);
  }

  // when event trigger call setState to rerender component
  handleMarkAsSeenLink() {
    this.props.onIconColor();
  }

  render() {
    return (
      <div>
        {this.props.items.map(item => (
          <div key={item.id}>
            <ListItem
              primaryText={
                <span style={{color: blue}}>{item.user.first_name} {item.user.last_name}</span>
              }
              secondaryText={
                <div>
                  <span style={{color: darkGrey}}>{item.body}</span>
                  <br style={{lineHeight: "20px"}}/>
                  <span style={{color: lightGrey}}>{moment(item.dates.created.date_time, "DD/MM/YYYY HH:mm").fromNow()}</span>
                  {!item.acknowledged && (
                    <MarkAsSeenLink onMarkAsSeenLink={this.handleMarkAsSeenLink} item={item}/>
                  )}
                </div>
              }
              secondaryTextLines={2}
              leftAvatar={<Avatar src={item.user.image.thumb_url} />}
              onClick={this.handleMarkAsSeenLink}
            />
            <Divider/>
          </div>
        ))}
      </div>
    )
  }
};

class ListMessages extends Component {
  constructor(props) {
    super(props);
    // define the state element that should render the component when changed
    this.state = {
      data: data
    };
    this.handleIconColor = this.handleIconColor.bind(this);
  }

  // when event trigger call setState to rerender component
  handleIconColor() {
    this.setState({
      data: data
    });
  }

  // compute the icon color based on the items acknowledged property value
  getIconColor() {
    const res = data.filter(item => !item.acknowledged);
    if (res.length) {
      return blue;
    }
    return lightGrey;
  }

  render() {
    return (
      <div>
        <IconMenu
          iconButtonElement={
            <IconButton
              touch={true}
              tooltip="messages"
              tooltipPosition="bottom-left"
            >
              <NotificationsIcon color={this.getIconColor()} />
            </IconButton>
          }
          anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
        >
          <ListMessagesContainer onIconColor={this.handleIconColor} items={data}/>
        </IconMenu>
      </div>
    );
  }
}

export default ListMessages;
